# Multi-node PostgreSQL HA deployment

This configuration deploys a multi-node GitLab instance with PostgreSQL failover
using Patroni, PGBouncer and Consul. This deploys the following nodes

1. 3x Consul nodes
1. 3x Patroni nodes
1. 1x node with GitLab rails and PGBouncer

Because [replication](https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html) requires manual intervention, this configuration differs from the documentation in the following ways:
1. Instead of using `md5` as authentication, we ship an hba file for PGBouncer,
   opening up connections from `127.0.0.1` with `trust` authentication method,
   thus bypassing the need of creating a `.pgpass` file for Consul user.

This directory also has a `Makefile` which helps in automating the manual
interventions. Run `make` to spin up the deployment and `make down` to spin it
down.

The GitLab image being used can be specified by `GITLAB_IMAGE` variable.

This configuration does not intentionally retain any configuration/data/log
directories in the host, as most of the time I spin up this instance, I want it
to come up from scratch.
