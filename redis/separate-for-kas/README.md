# Separate Redis for GitLab KAS

This configuration deploys a GitLab instance backed two Redis instances - one
for GitLab Rails and one for GitLab KAS.

The GitLab image being used can be specified by `GITLAB_IMAGE` variable.

This configuration does not intentionally retain any configuration/data/log
directories in the host, as most of the time I spin up this instance, I want it
to come up from scratch.
