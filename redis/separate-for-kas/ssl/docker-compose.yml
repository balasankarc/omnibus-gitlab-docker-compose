version: '3.8'
name: 'gitlab-test'

x-default_settings: &default_settings
  image: "${GITLAB_IMAGE:-gitlab/gitlab-ee:nightly}"
  pull_policy: always
  privileged: true
  shm_size: '256m'
  restart: always
  volumes:
    - "./ssl:/etc/gitlab/ssl"
    - "./trusted-certs:/etc/gitlab/trusted-certs"

services:
  redis-for-rails:
    <<: *default_settings
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        roles ['redis_master_role']

        # Redis configuration
        redis['bind'] = '10.0.0.2'
        redis['tls_port'] = 6380
        redis['tls_cert_file'] = '/etc/gitlab/ssl/self_signed.crt'
        redis['tls_key_file'] = '/etc/gitlab/ssl/self_signed.key'
        redis['tls_replication'] = 'yes'
        redis['password'] = 'toomanysecrets'
        # Workhorse won't work with client certificates. So disable client
        # authentication
        redis['tls_auth_clients'] = 'no'

        # Disable Redis non-TLS ports
        redis['port'] = 0

        # This is a pure Redis node. We don't need Rails stuff.
        gitlab_rails['enable'] = false
    networks:
      default:
        ipv4_address: 10.0.0.2
    healthcheck:
      test: ["CMD", "gitlab-redis-cli", "ping"]
      start_period: 60s

  redis-for-kas:
    <<: *default_settings
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        roles ['redis_master_role']

        # Redis configuration
        redis['bind'] = '10.0.0.3'
        redis['tls_port'] = 6380
        redis['tls_cert_file'] = '/etc/gitlab/ssl/self_signed.crt'
        redis['tls_key_file'] = '/etc/gitlab/ssl/self_signed.key'
        redis['tls_replication'] = 'yes'
        redis['tls_auth_clients'] = 'yes'
        redis['password'] = 'toomanysecrets'

        # Disable Redis non-TLS ports
        redis['port'] = 0

        # This is a pure Redis node. We don't need Rails stuff.
        gitlab_rails['enable'] = false
    networks:
      default:
        ipv4_address: 10.0.0.3
    healthcheck:
      test: ["CMD", "gitlab-redis-cli", "ping"]
      start_period: 60s

  gitlab:
    <<: *default_settings
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url "http://127.0.0.1"

        redis['enable'] = false

        # Redis related information for Rails
        gitlab_rails['redis_host'] = '10.0.0.2'
        gitlab_rails['redis_port'] = '6380'
        gitlab_rails['redis_password'] = 'toomanysecrets'
        gitlab_rails['redis_ssl'] = true

        # Redis related information for KAS
        gitlab_kas['redis_host'] = '10.0.0.3'
        gitlab_kas['redis_port'] = '6380'
        gitlab_kas['redis_password'] = 'toomanysecrets'
        gitlab_kas['redis_ssl'] = true
        gitlab_kas['redis_tls_client_cert_file'] = '/etc/gitlab/ssl/self_signed.crt'
        gitlab_kas['redis_tls_client_key_file'] = '/etc/gitlab/ssl/self_signed.key'
    ports:
      - "80:80"
      - "443:443"
      - "22:22"
    networks:
      default:
        ipv4_address: 10.0.0.4
    healthcheck:
      disable: true
    depends_on:
      redis-for-rails:
        condition: service_healthy
      redis-for-kas:
        condition: service_healthy

networks:
  default:
    ipam:
      config:
        - subnet: 10.0.0.0/24
