# Single Sentinel node

This configuration is used to test if the codebase related to Redis/Sentinel
works fine. It spins up two nodes - one being a Redis Master with Sentinel and
the other being GitLab application.

The GitLab image being used can be specified by `GITLAB_IMAGE` variable.

This configuration does not intentionally retain any configuration/data/log
directories in the host, as most of the time I spin up this instance, I want it
to come up from scratch.
