# Multi Sentinel node

This configuration deploys a GitLab instance backed by a multi-node Redis HA
instance. It spins up 4 nodes
1. 3x Redis + Sentinel nodes
1. 1x GitLab application node

The GitLab image being used can be specified by `GITLAB_IMAGE` variable.

This configuration does not intentionally retain any configuration/data/log
directories in the host, as most of the time I spin up this instance, I want it
to come up from scratch.
