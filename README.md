# Omnibus GitLab Docker Compose Configurations

This repository holds the docker-compose configurations that I use to test
various types of `omnibus-gitlab` based GitLab deployments.

Note that for these configurations to work, port 22 should be free for docker to
bind to. Make sure the host's SSH daemon is listening on a different port.

## SSL

The [`certs`](certs) directory in the root of this project contains self-signed
certificates that can be used in these deployments. The leaf certificate has
`localhost` as CN, and IP addresses ranging from `10.0.0.1` to `10.0.0.50` as
SANs, that should be sufficient for all the common deployment types. None of the
certificates have passwords on them.

Whenever possible, the deployments will have an `ssl` subdirectory that will
hold the deployment method, with components running over SSL.
